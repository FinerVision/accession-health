<?php

namespace Api\Handlers;

use Api\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;
use Laravel\Lumen\Exceptions\Handler;
use Throwable;

class ExceptionHandler extends Handler
{
    use ResponseTrait;

    /**
     * @inheritDoc
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * @inheritDoc
     *
     * @return JsonResponse
     */
    public function render($request, Throwable $exception): JsonResponse
    {
        //todo: extend this
        $class = get_class($exception);

        switch ($class) {
            default:
                return $this->generateResponse($exception->getMessage(), $exception->getCode());
        }
    }
}
