<?php

namespace Api\Traits;

interface ResponseInterface
{
    const MEDIA_TYPE = 'application/json';
    const HEADERS = [
        'Accept' => self::MEDIA_TYPE,
        'Content-type' => self::MEDIA_TYPE
    ];
}
