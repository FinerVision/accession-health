<?php

namespace Api\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class KeyGenerateCommand extends Command
{
    const ENV_FILE = '.env';

    /**
     * @inheritDoc
     */
    protected $signature = 'key:generate';

    /**
     * @inheritDoc
     */
    protected $description = "Set the application key";

    /**
     * @return void
     */
    public function handle(): void
    {
        $key = Str::random(64);
        $path = base_path(self::ENV_FILE);

        if (!file_exists($path)) {
            $this->error(sprintf('The %s file does not exist', self::ENV_FILE));
            return;
        }

        file_put_contents(
            $path,
            preg_replace(
                '/APP_KEY=(.*)\n/',
                sprintf("APP_KEY=base64:%s\n", $key),
                file_get_contents($path)
            )
        );

        $this->info("Application key has been created successfully.");
    }
}
