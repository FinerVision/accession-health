<?php

namespace Api\Console;

use Api\Console\Commands\KeyGenerateCommand;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * @inheritDoc
     */
    protected $commands = [
        KeyGenerateCommand::class,
    ];
}
