<?php

namespace Api\Dictionaries;

class Response
{
    const MESSAGE_ERROR = 'error';
    const MESSAGE_SUCCESS = 'success';
}
