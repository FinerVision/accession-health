<?php

use Api\Console\Kernel;
use Api\Handlers\ExceptionHandler;

require_once __DIR__.'/../vendor/autoload.php';

(new Laravel\Lumen\Bootstrap\LoadEnvironmentVariables(
    dirname(__DIR__)
))->bootstrap();

date_default_timezone_set(env('APP_TIMEZONE', 'UTC'));

$app = new Laravel\Lumen\Application(dirname(__DIR__));
$app->withFacades();
$app->withEloquent();

//bindings
$app->singleton(Illuminate\Contracts\Console\Kernel::class, Kernel::class);
$app->singleton(Illuminate\Contracts\Debug\ExceptionHandler::class, ExceptionHandler::class);

// load the config
$app->configure('app');

// middleware

// providers

// routes
$app->router->group([], function ($router) use ($app) {
    require_once __DIR__ . '/../routes/api.php';
});

return $app;
